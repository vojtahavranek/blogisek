<?php

namespace Article\Detail;

use Article\AbstractView;
use Article\ArticleDetail;
use Article\Components\Form;
use Article\Components\Input;
use Article\Components\TextArea;

class View extends AbstractView
{
    public function __construct(string $template)
    {
        $this->template = $template;
    }

    public function output(ArticleDetail $model): void
    {
        $article = $model->getArticle();
        $errors = $model->getErrors();
        if (!empty($errors)) {
            extract($errors);
        }
        extract($article);
        $action = '/article/detail/comment/';
        $action .= $article['article_id'];
        $commentForm = new Form( '', '', 'submitArticle('.$article['article_id'].')');
        $commentForm->addField(new Input('', 'hidden', 'article_id', $article['article_id']));
        $commentForm->addField(new Input('Jméno:', 'text', 'username', ''));
        $commentForm->addField(new TextArea('Komentář:', 'content', ''));
        $this->renderSelf([$article, ['comment_form' => $commentForm->build()]]);
    }
}