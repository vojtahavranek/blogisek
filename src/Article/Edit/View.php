<?php

namespace Article\Edit;

use Article\AbstractView;
use Article\ArticleEdit;
use Article\Components\Input;
use Article\Components\Form;
use Article\Components\TextArea;

class View extends AbstractView
{
    public function __construct(string $template)
    {
        $this->template = $template;
    }

    public function output(ArticleEdit $model)
    {
        $errors = $model->getErrors();
        if (!empty($errors)) {
            extract($errors);
        }
        $article = !empty($model->getArticle()) ? $model->getArticle() : ['title' => '', 'description' => '', 'content' => ''];
        $action = '/article/edit/submit/';
        if(isset($article['article_id'])) {
            $action .= $article['article_id'];
        }
        $editForm = new Form($action, 'post');
        $editForm->addField(new Input('Nadpis:', 'text', 'title', $article['title']));
        $editForm->addField(new TextArea('Popis:', 'description', $article['description']));
        $editForm->addField(new TextArea('Obsah:', 'content', $article['content']));
        $this->renderSelf([['form' => $editForm->build()]]);
    }
}