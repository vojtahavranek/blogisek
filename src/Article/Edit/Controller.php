<?php declare(strict_types=1);

namespace Article\Edit;

use Article\AbstractController;
use Article\ArticleEdit;

class Controller extends AbstractController
{
    public function create(ArticleEdit $articleEdit): ArticleEdit
    {
        $this->validateLogin();
        return $articleEdit;
    }

    public function modify(ArticleEdit $articleEdit): ArticleEdit
    {
        $this->validateLogin();
        if (empty($articleEdit->getId())) {
            return $articleEdit;
        }
        $articleEdit = $articleEdit->load();
        if ($articleEdit->getArticle() == false) {
            header('Location: /article/error');
        }
        return $articleEdit;
    }

    public function submit(ArticleEdit $articleEdit, array $params): ArticleEdit
    {
        $this->validateLogin();
        unset($params[0]);
        unset($params['submit']);
        $articleEdit = $articleEdit->save($params);
        if ($articleEdit->isSubmitted() && empty($articleEdit->getErrors())) {
            header('Location: /article/edit/modify/' . $articleEdit->getArticle()['article_id']);
        }
        return $articleEdit;
    }
}