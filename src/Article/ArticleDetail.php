<?php

namespace Article;

use DateTime;
use PDO;

class ArticleDetail
{
    private PDO $pdo;
    private int $id;
    private $errors = [];

    public function __construct(PDO $pdo, array $args)
    {
        $this->pdo = $pdo;
        $this->id = (int)$args[0];
        if (!empty($args[1]))
            $this->errors = $args[1];
    }

    public function setId(int $id): self
    {
        return new self($this->pdo, [$id, $this->errors]);
    }

    public function getArticle(): array
    {
        $query = $this->pdo->prepare('SELECT * FROM article WHERE article_id = :article_id;');
        $query->execute(['article_id' => $this->id]);
        $article = $query->fetch();
        if (is_array($article)) {
            return $article;
        } else {
            return [];
        }
    }

    public function addComment(array $comment): self
    {
        $errors = $this->validate($comment);
        if (empty($errors)) {
            $query = $this->pdo->prepare('INSERT INTO article_comment (article_id, username, content, insert_date) values (:article_id, :username, :content, :insert_date)');
            $now = new DateTime();
            $comment['insert_date'] = $now->getTimestamp();
            $query->execute($comment);
            return new self($this->pdo, [$this->id]);
        }
        return new self($this->pdo, [$this->id, $errors]);
    }

    private function validate(array $comment): array
    {
        $errors = [];
        if (empty($comment['username'])) {
            $errors[] = 'Jméno nesmí být prázdné';
        }
        if (empty($comment['content'])) {
            $errors[] = 'Obsah nesmí být prázdný';
        }
        return $errors;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

}