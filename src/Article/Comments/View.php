<?php


namespace Article\Comments;


use Article\ArticleComments;

class View
{

    public function output(ArticleComments $articleComments){
        $comments = $articleComments->getComments();
        for($i = 0; $i < sizeof($comments); $i++){
            $comments[$i]['content'] = htmlspecialchars($comments[$i]['content']);
            $comments[$i]['insert_date'] = date('j. n. Y H:i',$comments[$i]['insert_date']);
        }
        extract(['comments' =>$comments]);
        require(base_dir . '/src/templates/comments.phtml');
    }
}