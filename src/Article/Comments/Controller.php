<?php

namespace Article\Comments;

use Article\ArticleComments;

class Controller
{
    public function all(ArticleComments $articleComments): ArticleComments
    {
        return $articleComments;
    }
}