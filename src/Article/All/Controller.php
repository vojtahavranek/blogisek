<?php

namespace Article\All;

use Article\AbstractController;
use Article\ArticleAll;

class Controller extends AbstractController
{
    public function delete(ArticleAll $articlesList, array $params): ArticleAll
    {
        $this->validateLogin();
        return $articlesList->delete($params[0]);
    }

}