<?php

namespace Article\All;

use Article\AbstractView;
use Article\ArticleAll;

class View extends AbstractView
{
    public function __construct(string $template)
    {
        $this->template = $template;
    }

    public function output(ArticleAll $model): void
    {
        $articles = $model->getArticles();
        if ($this->template) {
            $this->renderSelf([['articles' => $articles], ['loggedIn' => $this->isLoggedIn()]]);
        }
    }
}