<?php
namespace Article\Error;

class View
{
    public function output($model){
        header('HTTP/1.0 404 Not Found');
        require(base_dir . '/src/templates/header.phtml');
        require(base_dir . '/src/templates/missing.phtml');
        require(base_dir . '/src/templates/footer.phtml');
    }
}