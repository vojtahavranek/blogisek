<?php

namespace Article;

use PDO;

class ArticleAll
{
    private PDO $pdo;
    private string $sort = 'newest';

    public function __construct(PDO $pdo, array $args)
    {
        $this->pdo = $pdo;
    }

    /**
     * Vrátí nový model s upraveným sortem.
     *
     * @param $sort
     * @return $this
     */
    public function sort($sort): self
    {
        return new self($this->pdo, $sort);
    }

    public function getSort(): string
    {
        return $this->sort;
    }

    /**
     * Vrátí články upravené podle toho, jaký máme momentálně nastavený sort.
     *
     * @return array
     */
    public function getArticles(): array
    {
        if ($this->sort == 'newest') {
            $order = 'ORDER BY article_id DESC';
        } else if ($this->sort == 'oldest') {
            $order = 'ORDER BY article_id ASC';
        } else {
            $order = '';
        }
        $query = $this->pdo->prepare('SELECT * FROM article ' . $order);
        $query->execute();
        return $query->fetchAll();
    }

    public function delete($id): self
    {
        $query = $this->pdo->prepare('DELETE FROM article WHERE article_id = :article_id');
        $query->execute(['article_id' => $id]);
        return $this;
    }
}