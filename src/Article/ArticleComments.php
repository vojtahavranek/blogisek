<?php


namespace Article;


use PDO;

class ArticleComments
{
    private PDO $pdo;
    private int $articleId;

    public function __construct($pdo, $args){
        $this->pdo = $pdo;
        $this->articleId = (int) $args[0];
    }

    public function getComments(): array
    {
        $query = $this->pdo->prepare('SELECT * FROM article_comment WHERE article_id = :article_id ORDER BY insert_date DESC;');
        $query->execute(['article_id' => $this->articleId]);
        return $query->fetchAll();
    }
}