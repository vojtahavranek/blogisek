<?php


namespace Article\Components;


interface Field
{
    public function build();
}