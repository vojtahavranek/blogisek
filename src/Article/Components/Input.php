<?php


namespace Article\Components;


class Input implements Field
{
    private string $label;
    private string $type;
    private string $name;
    private string $value;

    public function __construct($label, $type, $name, $value = ''){
        $this->label = $label;
        $this->type = $type;
        $this->name = $name;
        $this->value = $value;
    }

    public function build(){
        return "<label for='$this->name'>$this->label</label><input type='$this->type' name='$this->name' id='$this->name' value='$this->value'/>";
    }
}