<?php


namespace Article\Components;


class Form
{
    private array $fields;
    private string $action;
    private string $method;
    private string $onsubmit;

    public function __construct($action, $method, $onsubmit = ''){
        $this->action = $action;
        $this->method = $method;
        $this->onsubmit = $onsubmit;
    }

    public function addField(Field $field) {
        $this->fields[] = $field;
    }

    public function build() {
        $form = "<form id='kokotinka' name='kokotinka'" .
            (!empty($this->action)?"action='$this->action' ":"")
            . (!empty($this->method)?"method='$this->method' ":"") . ">";
        foreach($this->fields as $field){
            $form .= $field->build() . '<br />';
        }
        $form .= "<input " .(!empty($this->onsubmit)?"type='button'":"type='submit'")." value='Odeslat' name='submit'" . (!empty($this->onsubmit)?"onclick='$this->onsubmit' ":"")."/>";
        $form .= "</form>";
        return $form;
    }
}