<?php


namespace Article;


abstract class AbstractController
{
    public function validateLogin()
    {
        if (!isset($_COOKIE['sessionid'])) {
            header('location: /user/login');
        }
    }
}