<?php

namespace Article;

use PDO;

class ArticleEdit
{
    private PDO $pdo;
    private int $id = -1;
    private array $article = [];
    private bool $submitted = false;
    private array $errors = [];

    public function __construct(PDO $pdo, array $params)
    {
        $this->pdo = $pdo;
        if (isset($params[0]) && ctype_digit($params[0]))
            $this->id = (int)$params[0];
        if (isset($params['id']))
            $this->id = $params['id'];
        if (isset($params['article']))
            $this->article = $params['article'];
        if (isset($params['submitted']))
            $this->submitted = $params['submitted'];
        if (isset($params['errors']))
            $this->errors = $params['errors'];
    }

    public function load(): self
    {

        $query = $this->pdo->prepare('SELECT * FROM article WHERE article_id = :article_id');
        $query->execute(['article_id' => $this->id]);
        $article = $query->fetch();
        return new self($this->pdo, ['id' => $this->id, 'article' => $article, 'submitted' => $this->submitted]);
    }

    public function save($article): self
    {
        $errors = $this->validate($article);
        if (!empty($errors)) {
            return new self($this->pdo, ['id' => $this->id, 'article' => $article, 'submitted' => true, 'errors' => $errors]);
        }
        if ($this->id > 0) {
            return $this->update($article);
        } else {
            return $this->insert($article);
        }
    }

    private function validate(array $article): array
    {
        $errors = [];
        if (empty($article['title'])) {
            $errors[] = 'Nadpis nesmí být prázdný.';
        }
        if (empty($article['description'])) {
            $errors[] = 'Popis nesmí být prázdný.';
        }
        if (empty($article['content'])) {
            $errors[] = 'Obsah nesmí být prázdný.';
        }
        return $errors;
    }

    private function update(array $article): self
    {
        $query = $this->pdo->prepare('UPDATE article SET title = :title, description = :description, content = :content WHERE article_id = :article_id');
        $article['article_id'] = (int) $this->id;
        $query->execute($article);
        return new self($this->pdo, ['id' => $this->id, 'article' => $article, 'submitted' => true]);
    }

    private function insert(array $article): self
    {
        $query = $this->pdo->prepare('INSERT into ARTICLE (title, description, content) VALUES (:title, :description, :content)');
        $query->execute($article);
        $article['article_id'] = $this->pdo->lastInsertId();
        return new self($this->pdo, ['id' => $article['article_id'], 'article' => $article, 'submitted' => true]);
    }

    public function getArticle(): array
    {
        return $this->article;
    }

    public function isSubmitted(): bool
    {
        return $this->submitted;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function getId(): int
    {
        return $this->id;
    }

}