<?php


namespace Article;


abstract class AbstractView
{
    protected string $template;

    public function isLoggedIn()
    {
        return isset($_COOKIE['sessionid']);
    }

    protected function renderSelf($extracts){
        foreach($extracts as $extract) {
            extract($extract);
        }
        extract($_COOKIE);
        extract(['loggedIn' => $this->isLoggedIn()]);
        require(base_dir.'/src/templates/header.phtml');
        require(base_dir . '/src/templates/'.$this->template.'.phtml');
        require(base_dir . '/src/templates/footer.phtml');
    }
}