<?php
namespace User\Login;

use Article\AbstractView;
use User\UserLogin;

class View extends AbstractView
{
    public function output(UserLogin $userLogin){
        $this->template = 'login';
        if(!empty($userLogin->getErrors())) {
            $errors = $userLogin->getErrors();
            var_dump($errors);
            $this->renderSelf(['errors' => $errors]);
            return;
        }
        $this->renderSelf([]);
    }
}