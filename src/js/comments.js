async function submitArticle(articleId){
    let formData = new FormData(document.getElementById('kokotinka'));
    await fetch('/article/detail/comment/' + articleId, {method: 'POST', body: formData});
    await loadArticles(articleId);
    document.forms['kokotinka']['username'].value = "";
    document.forms['kokotinka']['content'].value = "";

}

async function loadArticles(article_id) {
    let request = await fetch('/article/comments/all/' + article_id);
    document.getElementById('comments').innerHTML = await request.text();
}