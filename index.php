<?php
mb_internal_encoding('UTF-8');

require __DIR__ . '/vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/src/templates');
$twig = new \Twig\Environment($loader, [
    'cache' => __DIR__ . '/var/cache/twig/compilation_cache',
]);

// ukazka pouziti twig knihovny
/*echo $twig->render('test.html.twig', [
    'title' => 'My title',
    'test' => 'Hello test!',
]);*/

const base_dir = 'C:/xampp/htdocs/';

$pdo = new PDO('mysql:host=localhost;dbname=mvc_db', 'root', '');

function autoload($class) {
    $base_dir = __DIR__ . '/src/';
    $file = $base_dir . str_replace('\\', '/', $class) . '.php';
    if(file_exists($file)){
        require $file;
    }
}

spl_autoload_register('autoload');

$router = new Router($pdo);
$router->route();

